package com.cfperuweb.laboratorio01.view;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.cfperuweb.laboratorio01.R;
import com.cfperuweb.laboratorio01.databinding.ActivityMainBinding;
import com.cfperuweb.laboratorio01.model.APIError;
import com.cfperuweb.laboratorio01.model.Plato;
import com.cfperuweb.laboratorio01.networking.EndPoint;
import com.cfperuweb.laboratorio01.networking.HelperWs;
import com.cfperuweb.laboratorio01.networking.request.PlatoRequest;
import com.cfperuweb.laboratorio01.util.ErrorUtil;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    ActivityMainBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        initComponents();
    }

    private void initComponents() {
        binding.btnListaPlatos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listarPlatos();
            }
        });

        binding.btnPlato.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                obtenerPlato();
            }
        });

        binding.btnGrabarPlato.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                grabarPlato();
            }
        });

        binding.btnActualizarPlato.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                actualizarPlato();
            }
        });

        binding.btnEliminarPlato.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                eliminarPlato();
            }
        });

    }

    private void eliminarPlato() {
        EndPoint endPoint = HelperWs.getConfigurationRetrofit().create(EndPoint.class);
        Call<ArrayList<Plato>> response = endPoint.eliminarPlato("5f36f322c6ead70400ecf905");
        response.enqueue(new Callback<ArrayList<Plato>>() {
            @Override
            public void onResponse(Call<ArrayList<Plato>> call, Response<ArrayList<Plato>> response) {
                if(response.isSuccessful()){
                    Toast.makeText(MainActivity.this, "Se eliminó Correctamente", Toast.LENGTH_SHORT).show();
                    ArrayList<Plato> listaPlato = response.body();
                    try{
                        for (Plato plato: listaPlato){
                            Log.i("cindy", plato.get_id());
                            Log.i("cindy", plato.getIdCategoria());
                            Log.i("cindy", plato.getNombre());
                            Log.i("cindy", plato.getDescripcion());
                            Log.i("cindy", plato.getFoto());
                            Log.i("cindy", String.valueOf(plato.getPrecio()));
                            Log.i("cindy", String.valueOf(plato.get__v()));
                        }
                    }catch (Exception ex){
                        Log.e("Cindy", "onResponse: "+ex.getMessage() );
                    }
                }
            }

            @Override
            public void onFailure(Call<ArrayList<Plato>> call, Throwable t) {
                Toast.makeText(MainActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void actualizarPlato() {
        //cbo_categoria_plato ,txt_nombre_plato , txt_precio_plato, text_descripcion_plato, idPlato
        PlatoRequest platoRequest = new PlatoRequest();
        platoRequest.setIdPlato("5f36f322c6ead70400ecf905");
        platoRequest.setCbo_categoria_plato("? undefined:undefined ?");
        platoRequest.setTxt_nombre_plato("Aguadito Norteño");
        platoRequest.setText_descripcion_plato("Consiste en una sopa de pollo espesa con arroz y otros vegetales. De una coloración verde debida al uso significativo de culantro en la sopa, es consumido tradicionalmente en invierno.");
        platoRequest.setTxt_precio_plato("18");


        EndPoint endPoint = HelperWs.getConfigurationRetrofit().create(EndPoint.class);
        Call<ArrayList<Plato>> response = endPoint.actualizarPlato(platoRequest);
        response.enqueue(new Callback<ArrayList<Plato>>() {
            @Override
            public void onResponse(Call<ArrayList<Plato>> call, Response<ArrayList<Plato>> response) {
                if(response.isSuccessful()){
                    ArrayList<Plato> listaPlato = response.body();
                    try{
                        for (Plato plato: listaPlato){
                            Log.i("cindy", plato.get_id());
                            Log.i("cindy", plato.getIdCategoria());
                            Log.i("cindy", plato.getNombre());
                            Log.i("cindy", plato.getDescripcion());
                            Log.i("cindy", plato.getFoto());
                            Log.i("cindy", String.valueOf(plato.getPrecio()));
                            Log.i("cindy", String.valueOf(plato.get__v()));
                        }
                    }catch (Exception ex){
                        Log.e("Cindy", "onResponse: "+ex.getMessage() );
                    }
                }
            }

            @Override
            public void onFailure(Call<ArrayList<Plato>> call, Throwable t) {
                Toast.makeText(MainActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void grabarPlato() {
        PlatoRequest platoRequest = new PlatoRequest();
        platoRequest.setCbo_categoria_plato("? undefined:undefined ?");
        platoRequest.setTxt_nombre_plato("Aguadito");
        platoRequest.setText_descripcion_plato("Consiste en una sopa de pollo espesa con arroz y otros vegetales. De una coloración verde debida al uso significativo de culantro en la sopa, es consumido tradicionalmente en invierno.");
        platoRequest.setTxt_precio_plato("10");

        EndPoint endPoint = HelperWs.getConfigurationRetrofit().create(EndPoint.class);
        Call<ArrayList<Plato>> response = endPoint.grabarPlato(platoRequest);
        response.enqueue(new Callback<ArrayList<Plato>>() {
            @Override
            public void onResponse(Call<ArrayList<Plato>> call, Response<ArrayList<Plato>> response) {
                if(response.isSuccessful()){
                    if(response.code() == 200){
                        ArrayList<Plato> listaPlato = response.body();

                        try{
                            for(Plato plato : listaPlato){
                                Log.i("cindy", plato.get_id());
                                Log.i("cindy", plato.getIdCategoria());
                                Log.i("cindy", plato.getNombre());
                                Log.i("cindy", plato.getDescripcion());
                                Log.i("cindy", plato.getFoto());
                                Log.i("cindy", String.valueOf(plato.getPrecio()));
                                Log.i("cindy", String.valueOf(plato.get__v()));
                            }
                        }catch (Exception ex){
                            Log.e("Cindy", "onResponse: "+ex.getMessage() );
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<ArrayList<Plato>> call, Throwable t) {
                Toast.makeText(MainActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void obtenerPlato() {
        EndPoint endPoint = HelperWs.getConfigurationRetrofit().create(EndPoint.class);
        Call<Plato> response = endPoint.obtenerPlato("5797764e946ff80300471893");
        response.enqueue(new Callback<Plato>() {
            @Override
            public void onResponse(Call<Plato> call, Response<Plato> response) {
                if(response.isSuccessful()){
                    if(response.code() == 200){
                        Plato plato = response.body();
                        Log.i("cindy", plato.get_id());
                        Log.i("cindy", plato.getIdCategoria());
                        Log.i("cindy", plato.getNombre());
                        Log.i("cindy", plato.getDescripcion());
                        Log.i("cindy", plato.getFoto());
                        Log.i("cindy", String.valueOf(plato.getPrecio()));
                        Log.i("cindy", String.valueOf(plato.get__v()));

                    }
                }
            }

            @Override
            public void onFailure(Call<Plato> call, Throwable t) {
                Toast.makeText(MainActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });
    }

    private void listarPlatos() {
        EndPoint endPoint = HelperWs.getConfigurationRetrofit().create(EndPoint.class);
        Call<ArrayList<Plato>> response = endPoint.obtenerPlatos();
        response.enqueue(new Callback<ArrayList<Plato>>() {
            @Override
            public void onResponse(Call<ArrayList<Plato>> call, Response<ArrayList<Plato>> response) {
                if(response.isSuccessful()){

                    try{
                        ArrayList<Plato> listaPlatos = response.body();
                        for(Plato plato : listaPlatos){
                            Log.i("cindy", plato.get_id());
                            Log.i("cindy", plato.getIdCategoria());
                            Log.i("cindy", plato.getNombre());
                            Log.i("cindy", plato.getDescripcion());
                            Log.i("cindy", plato.getFoto());
                            Log.i("cindy", String.valueOf(plato.getPrecio()));
                            Log.i("cindy", String.valueOf(plato.get__v()));
                        }
                    }catch (Exception ex){
                        Log.e("Cindy", "onResponse: "+ex.getMessage() );
                    }


                }else{
                    APIError apiError = ErrorUtil.parseError(response);
                    Toast.makeText(MainActivity.this, apiError.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ArrayList<Plato>> call, Throwable t) {
                Toast.makeText(MainActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }


}