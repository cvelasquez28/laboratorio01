package com.cfperuweb.laboratorio01.model;

public class APIError {

    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
