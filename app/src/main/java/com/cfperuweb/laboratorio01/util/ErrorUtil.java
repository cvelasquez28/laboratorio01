package com.cfperuweb.laboratorio01.util;

import com.cfperuweb.laboratorio01.model.APIError;
import com.cfperuweb.laboratorio01.networking.HelperWs;

import java.io.IOException;
import java.lang.annotation.Annotation;

import okhttp3.ResponseBody;
import retrofit2.Converter;
import retrofit2.Response;

public class ErrorUtil {

    public static APIError parseError(Response<?> response){
        Converter<ResponseBody,APIError> converter =
                HelperWs.getConfigurationRetrofit()
                .responseBodyConverter(APIError.class, new Annotation[0]);

        APIError error;

        try {
            error = converter.convert(response.errorBody());
        }catch (IOException e){
            return new APIError();
        }
        return  error;
    }
}
