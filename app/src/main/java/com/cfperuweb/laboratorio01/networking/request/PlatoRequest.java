package com.cfperuweb.laboratorio01.networking.request;

public class PlatoRequest {

    private String idPlato;
    private String cbo_categoria_plato; //? undefined:undefined ?
    private String txt_nombre_plato;
    private String txt_precio_plato;
    private String text_descripcion_plato;
    private String txt_foto_plato;

    public String getIdPlato() {
        return idPlato;
    }

    public void setIdPlato(String idPlato) {
        this.idPlato = idPlato;
    }

    public String getCbo_categoria_plato() {
        return cbo_categoria_plato;
    }

    public void setCbo_categoria_plato(String cbo_categoria_plato) {
        this.cbo_categoria_plato = cbo_categoria_plato;
    }

    public String getTxt_nombre_plato() {
        return txt_nombre_plato;
    }

    public void setTxt_nombre_plato(String txt_nombre_plato) {
        this.txt_nombre_plato = txt_nombre_plato;
    }

    public String getTxt_precio_plato() {
        return txt_precio_plato;
    }

    public void setTxt_precio_plato(String txt_precio_plato) {
        this.txt_precio_plato = txt_precio_plato;
    }

    public String getText_descripcion_plato() {
        return text_descripcion_plato;
    }

    public void setText_descripcion_plato(String text_descripcion_plato) {
        this.text_descripcion_plato = text_descripcion_plato;
    }

    public String getTxt_foto_plato() {
        return txt_foto_plato;
    }

    public void setTxt_foto_plato(String txt_foto_plato) {
        this.txt_foto_plato = txt_foto_plato;
    }
}
