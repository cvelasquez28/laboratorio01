package com.cfperuweb.laboratorio01.networking;

import com.cfperuweb.laboratorio01.model.Plato;
import com.cfperuweb.laboratorio01.networking.request.PlatoRequest;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface EndPoint {

    @GET("plato")
    Call<ArrayList<Plato>> obtenerPlatos();

    @GET("plato/{idPlato}")
    Call<Plato> obtenerPlato(@Path("idPlato") String idPlato);

    @POST("plato")
    @Headers("Content-Type:application/json")
    Call<ArrayList<Plato>> grabarPlato(@Body PlatoRequest platoRequest);

    @PUT("plato")
    @Headers("Content-Type:application/json")
    Call<ArrayList<Plato>> actualizarPlato(@Body PlatoRequest platoRequest);

    @DELETE("plato/{idPlato}")
    Call<ArrayList<Plato>> eliminarPlato (@Path("idPlato") String idPlato );

}
